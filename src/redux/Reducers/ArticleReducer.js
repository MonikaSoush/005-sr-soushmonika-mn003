const initialState = {
    articles: [],
    view: [],
    totalPage: null
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

    case "FETCH_ARTICLE":
        return { ...state, articles: [...payload.data], totalPage: payload.total_page }

    case "DELETE_ARTICLE":
        return { ...state, articles: state.articles.filter(item => item._id !== payload._id) }

    case "POST_ARTICLE":
    return { ...state, payload }

    case "UPLOAD_IMG":
        return { ...state, payload }

    case "FETCH_ARTICLE_BY_ID":
    return { ...state, view: payload }

    case "UPDATE_ARTICLE_BY_ID":
    return { ...state, payload }

    default:
        return state
    }
}