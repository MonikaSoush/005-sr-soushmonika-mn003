import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteAuthor, fetchAuthor, postAuthor, updateAuthor } from "../redux/Action/AuthorAction";
import { Container, Form, Row, Col, Button, Table } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { uploadImage } from "../redux/Action/ArticleAction";
import Swal from 'sweetalert2'
import { strings } from "../localization/localize";


export default function Author() {
  const [imageURL, setImageURL] = useState(
    "https://designshack.net/wp-content/uploads/placeholder-image.png"
  );
  const [imageFile, setImageFile] = useState(null);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [isUpdate, setIsUpdate] = useState(null);
  const [isRefresh, setIsRefresh] = useState(false);

  const dispatch = useDispatch();
  const onDelete = bindActionCreators(deleteAuthor, dispatch);
  const onUploadImg = bindActionCreators(uploadImage, dispatch);
  const onPost = bindActionCreators(postAuthor, dispatch);
  const onUpdateAuthor= bindActionCreators(updateAuthor, dispatch);
  const state = useSelector((state) => state.AuthorReducer.authors);

  useEffect(() => {
    dispatch(fetchAuthor());
    setIsRefresh(false)
  }, [isRefresh]);

  const onAdd = async (e) => {
    e.preventDefault();
    let newAuthor = { name, email };

    if (imageFile) {
      let url = await onUploadImg(imageFile);
      newAuthor.image = url.payload;
    }
    onPost(newAuthor).then((message) => {
        setIsRefresh(true)
        Swal.fire({
            position: 'top',
            icon: 'success',
            title: message.payload,
            showConfirmButton: false,
            timer: 1500
          })
    });
    setImageFile(null);
    setName('');
    setEmail('');
    clear();
  };

  function clear(){
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("img").value = "";
    document.getElementById("image").src =
      "https://designshack.net/wp-content/uploads/placeholder-image.png";
  }

  const onEdit = (id, name, email, image) => {
    setIsUpdate(id)
    setName(name)
    setEmail(email)
    document.getElementById("name").value = name;
    document.getElementById("email").value = email;
    document.getElementById("image").src = image;
  };

  const onUpdate = async (e) => {
    e.preventDefault();
    let newAuthor = { name, email };

    if (imageFile) {
        let url = await onUploadImg(imageFile);
        newAuthor.image = url.payload;
      }

    onUpdateAuthor(isUpdate, newAuthor).then((message) => {
        Swal.fire({
            position: 'top',
            icon: 'success',
            title: message.payload,
            showConfirmButton: false,
            timer: 1500
          })
    });
    setIsRefresh(true)
    setIsUpdate(null);
    setImageFile(null);
    setName('');
    setEmail('');
    clear();
  };

  return (
    <div>
      <Container>
        <h1>{strings.Author}</h1>
        <Row>
          <Col md={8}>
            <Form>
              <Form.Group>
                <Form.Label>{strings.Author}</Form.Label>
                <Form.Control
                  id="name"
                  type="text"
                  placeholder="Author Name"
                  onChange={(e) => setName(e.target.value)}
                />
                <Form.Text className="text-muted"></Form.Text>
              </Form.Group>

              <Form.Group>
                <Form.Label>{strings.Email}</Form.Label>
                <Form.Control
                  id="email"
                  type="text"
                  placeholder="Email"
                  onChange={(e) => setEmail(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Button
                variant="primary"
                type="submit"
                disabled={
                  name === '' && email === '' || 
                  name !== '' && email === '' ||
                  name === '' && email !== ''
                }
                onClick={isUpdate !== null ? onUpdate : onAdd}
              >
                {isUpdate !== null ? strings.Save : strings.Add}
              </Button>
            </Form>
          </Col>
          <Col md={4}>
            <img id="image" className="w-75" alt="..." src={imageURL} />
            <Form>
              <Form.Group>
                <Form.File
                  id="img"
                  label={strings.ChooseImage}
                  onChange={(e) => {
                    let url = URL.createObjectURL(e.target.files[0]);
                    setImageFile(e.target.files[0]);
                    setImageURL(url);
                  }}
                />
              </Form.Group>
            </Form>
          </Col>
        </Row>

        <Table striped bordered hover>
          <thead>
            <tr>
              <th>ID</th>
              <th>{strings.Name}</th>
              <th>{strings.Email}</th>
              <th>{strings.Img}</th>
              <th>{strings.Action}</th>
            </tr>
          </thead>
          <tbody>
            {state.map((item, index) => (
              <tr key={index}>
                <td>{item._id}</td>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td>
                  <img src={item.image} alt="" width="auto" height={100} />
                </td>
                <td>
                  <Button
                    size="sm"
                    variant="warning"
                    onClick={() =>
                      onEdit(item._id, item.name, item.email, item.image)
                    }
                  >
                    {strings.Edit}
                  </Button>{" "}
                  <Button
                    size="sm"
                    variant="danger"
                    onClick={() => onDelete(item._id)}
                  >
                    {strings.Delete}
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </div>
  );
}
