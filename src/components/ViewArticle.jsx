import React, { useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { fetchArticleById } from "../redux/Action/ArticleAction";
import ReactUtterences from "react-utterances";

export default function ViewArticle() {
  let { id } = useParams();

  const dispatch = useDispatch();
  const state = useSelector((state) => state.ArticleReducer.view);

  useEffect(() => {
    dispatch(fetchArticleById(id));
  }, []);

  
const repo = 'github-namespace/project-name'

  return (
    <Container>
      {state ? (
        <>
          <Row>
            <Col md={8}>
              <h1 className="my-2">{state.title}</h1>
              <img
                width="500"
                height="100%"
                alt="..."
                src={
                  state.image
                    ? state.image
                    : "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"
                }
              />
              <p>{state.description}</p>
            </Col>
            <Col md={4}>
              {/* <div>
                <h1>What is Lorem Ipsum?</h1>
                <p>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry.
                </p>
                <ReactUtterences repo={repo} type={"pathname"} />
              </div> */}
            </Col>
          </Row>
        </>
      ) : (
        <h1>Loading...</h1>
      )}
    </Container>
  );
}
